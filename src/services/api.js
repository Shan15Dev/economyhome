import { getFirestore, collection, getDocs } from 'firebase/firestore';
import { app } from './firebase/FirebaseConfig';



const db = getFirestore(app);

export async function getTypes() {
  const typeCollection = collection(db, 'types');
  const typesSnapshot = await getDocs(typeCollection);
  const typesList = typesSnapshot.docs.map(doc => doc.data());
  return typesList;
}