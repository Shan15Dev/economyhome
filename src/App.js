import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Nav from "./components/Nav";
import Home from './pages/Home';

function App() {
  return (
    <Router>
      <div>
        <Nav />
        <Routes>
          <Route path="/" exact element={<Home />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
